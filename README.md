# Air Quality Index (AQI) – Line Flex Message

## Table of Contents

- [Project Overview](#markdown-header-project-overview)
- [Objectives](#markdown-header-objectives)
- [Guildline](#markdown-header-guildline)
- [Resources](#markdown-header-resources)
- [Development](#markdown-header-development)

## Project Overview
In this project, you will learn how to fetch Air Quality Index (AQI) data via exposed AirVisual API. Then, the AQI data will be broadcast to all subscriber displaying in a type of Line Flex message. At this step, you will learn how to initialize a Line Channel, configure a webhook, and then use Line Message API to send a message to subscribed users. Moreover, since AQI data should be sent at a specific interval (9am, 12pm, and 8pm every day), you will use a capability of Laravel Task Scheduling which is able to call HTTP endpoint at any interval.

## Objectives
After you finish this project, you should be able to:

- Set up a pre-configured project using Docker Compose
- Create a simple API using Laravel Routing / Controller
- Fetch data from an external API
- Set up a Line Channel via Line developer account
- Configure a webhook for bidirectional communication between Line bot and a server
- Use Line Message API to send a message to a specific Line user or group
- Use Laravel Task Scheduling to configure a cron job
- Connect, store, and fetch data to/from a database

## Guideline
1. Clone this repository to your local machine.
2. Follow the Development section below to initialize a Laravel application.
3. Follow the following guideline to create a Line channel. https://developers.line.biz/en/docs/messaging-api/getting-started/ 
4. Create a controller in `app/Http/Controllers/API`.
5. Add a method to the recently created controller. This method will be responsible for:
    - Receiving an HTTP POST request that contains a webhook event object from Line platform
    - Validating the signature `X-Line-Signature` in the request header to ensure that this request is sent from the Line platform
    - Checking whether the webhook event is a type of "join" or not
    - Saving a userID associated in this webhook object to the `subscriber` table in the database only if it has a type of "join"
6. Add another method that: 
    - Fetches AQI data from AirVisual API: https://www.airvisual.com/air-pollution-data-api
    - Sends a PM2 value extracted from a response to registered subscribers (all userID stored in the `subscriber` table)
    - Ensure that you send it using a Flex Message, not plain text. An example of a Flex message is provided below.
    ![Flex Message Example](./readme/images/flex-message-example.png)
7. Create two HTTP endpoints in `routes/api.php` linking to the two methods that you created previously.
8. Configure Laravel Task Scheduling to call the HTTP endpoint which links to the method you created in 6. Then, set it to be constantly called at 9am, 12pm, and 8pm every day.

## Resources
- Laravel Task Scheduing: https://laravel.com/docs/5.7/scheduling
- Building a Line bot: https://developers.line.biz/en/docs/messaging-api/building-bot
- Using Flex Messages: https://developers.line.biz/en/docs/messaging-api/using-flex-messages
- Flex Message Simulator: https://developers.line.biz/console/fx
- Air quality (AQI) and pollution API: https://www.airvisual.com/air-pollution-data-api

## Development
Although it is not required to develop the application on Docker, using it can keep the environment the same on all machines and can solve "but it is working on my machine" problem. However, Windows user may find that handling with Docker seems very tedious due to buggy issues on Docker for Windows per se, so using local machine sounds more convenient in this case.

**Note that** this Docker's configuration uses volume to map files to container and it is going to make the application feel **sluggish** in the browser — a few hundred ms lag (especially on macOS), but the speed issues will no longer be there on production mode.

### Prerequisites
- [Docker Community Edition](https://www.docker.com/community-edition) `17.12.0-ce` or higher
- [Docker Compose](https://docs.docker.com/compose/install) `1.18.0` or higher
- [Yarn](https://yarnpkg.com/en/docs/install) `1.3.2` or higher (a `npm` replacement)

### Quickstart
The following instruction will guide you in building the application from scratch. Please note that all command should be run in project's root directory.

1). Build a workspace Docker image for dependency's installation.
```shell
yarn run build:workspace
```
> This step may take some time. Please be patient.

2). Install PHP and Node dependencies by running the pre-built workspace Docker container.
```shell
yarn run workspace
```
> For Windows user, `yarn run workspace` may not work properly due to an environment variable in yarn script. Try running `docker run -it -v $PWD:/var/www aqi-line-flex-message-workspace` and replacing `$PWD` with your project's root directory instead.

3). Duplicate `.env.docker.example` (not `.env.example`) and rename the copy to `.env`.

4). Generate an application key and run the optimize inside the pre-built workspace Docker container.
```shell
yarn run workspace php artisan key:generate
yarn run workspace php artisan optimize
```
> For Windows user, `yarn run workspace` may not work properly due to an environment variable in yarn script. Try running `docker run -it -v $PWD:/var/www aqi-line-flex-message-workspace php artisan [COMMAND]` and replacing `$PWD` with your project's root directory instead.

5). Start the services by running `docker-compose up`.

6). Import data to database. See [Importing Data](#importing-data) below.

Now, the application should be available at `http://localhost:8081`

### Developing

#### Running Artisan, Yarn, and Composer
Any time you want to run a command, please execute it inside the pre-built workspace container by appending `yarn run workspace` to the command you want to run. For example,
```shell
yarn run workspace yarn
yarn run workspace composer update
yarn run workspace php artisan key:generate
```
> For Windows user, `yarn run workspace` may not work properly due to an environment variable in yarn script. Try running `docker run -it -v $PWD:/var/www aqi-line-flex-message-workspace [COMMAND]` and replacing `$PWD` with your project's root directory instead.

#### Starting Docker
```shell
docker-compose up
```

#### Stopping Docker
Hold `control` and press `c`.

If you want to stops containers and removes containers, networks, volumes, and images created by up, use the following command.
```shell
docker-compose down
```

#### Importing Data
At this step, you can use any MySQL database management application you prefers such as Sequel Pro. Connect to the database via an exposed port (`127.0.0.1:33062`) using the following credential. Then, import data directly.

- **Username:** noman
- **Password:** secret
- **Database Name:** main

## Built With

* [Laravel](https://laravel.com/) - The PHP Framework For Web Artisans
* [Docker](https://www.docker.com/) - Software containerization platform

## Authors

* **Globish Engineer Team**

